package com.devcamp.s20_stringapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S20StringapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(S20StringapiApplication.class, args);
	}

}
