package com.devcamp.s10_schoolclassapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10_schoolclassapi.model.Classroom;
import com.devcamp.s10_schoolclassapi.service.ClassroomService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ClassController {
    @Autowired
    private ClassroomService classroomService;

    @GetMapping("/classes")
    public ArrayList<Classroom> getAllClasses(){
        return classroomService.getAllClasses();
    }

    @GetMapping("/nostudent")
    public ArrayList<Classroom> getClassByNoStudent(@RequestParam(name = "noStudent", required = true) int inpNumber){
        return classroomService.getClassByInputNoStudent(inpNumber);
    }
}
