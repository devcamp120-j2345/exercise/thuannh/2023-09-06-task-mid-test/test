package com.devcamp.s10_schoolclassapi.controller;

import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10_schoolclassapi.model.School;
import com.devcamp.s10_schoolclassapi.service.SchoolService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class SchoolController {
    @Autowired
    SchoolService schoolService;

    @GetMapping("/schools")
    public ArrayList<School> getAllSchoolApi(){
        return schoolService.getAllSchools();
    }
    
    @GetMapping("/school-by-id/{id}")
    public School getSchoolByIdApi(@PathVariable int id){
        return schoolService.getSchoolById(id);
    }

    @GetMapping("/school-by-number/{noNumber}")
    public ArrayList<School> getSchoolByNumberApi(@PathVariable int noNumber){
        return schoolService.getSchoolByInputNoStudent(noNumber);
    }
}
