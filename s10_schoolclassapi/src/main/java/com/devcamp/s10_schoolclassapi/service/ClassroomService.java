package com.devcamp.s10_schoolclassapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s10_schoolclassapi.model.Classroom;

@Service
public class ClassroomService {
    Classroom classroom1 = new Classroom(101, "Lớp 12A1", 46);
    Classroom classroom2 = new Classroom(102, "Lớp 12A2", 48);
    Classroom classroom3 = new Classroom(103, "Lớp 12A3", 45);

    Classroom classroom4 = new Classroom(104, "Lớp 12A4", 49);
    Classroom classroom5 = new Classroom(105, "Lớp 12A5", 50);
    Classroom classroom6 = new Classroom(106, "Lớp 12A6", 50);

    Classroom classroom7 = new Classroom(107, "Lớp 12A7", 48);
    Classroom classroom8 = new Classroom(108, "Lớp 12A8", 48);
    Classroom classroom9 = new Classroom(109, "Lớp 12A9", 46);

    public ArrayList<Classroom> getGroup1School(){
        ArrayList<Classroom> group1 = new ArrayList<>();

        group1.add(classroom1);
        group1.add(classroom2);
        group1.add(classroom3);

        return group1;
    }
    public ArrayList<Classroom> getGroup2School(){
        ArrayList<Classroom> group2 = new ArrayList<>();

        group2.add(classroom4);
        group2.add(classroom5);
        group2.add(classroom6);

        return group2;
    }
    public ArrayList<Classroom> getGroup3School(){
        ArrayList<Classroom> group3 = new ArrayList<>();

        group3.add(classroom8);
        group3.add(classroom7);
        group3.add(classroom9);

        return group3;
    }
    
    public ArrayList<Classroom> getAllClasses(){
        ArrayList<Classroom> allClasses = new ArrayList<>();
        allClasses.add(classroom1);
        allClasses.add(classroom2);
        allClasses.add(classroom3);
        allClasses.add(classroom4);
        allClasses.add(classroom5);
        allClasses.add(classroom6);
        allClasses.add(classroom7);
        allClasses.add(classroom8);
        allClasses.add(classroom9);

        return allClasses;
    }
    public ArrayList<Classroom> getClassByInputNoStudent(int inpNumber){
        ArrayList<Classroom> target = new ArrayList<>();
        for (int i=0; i<getAllClasses().size(); i++){
            if (getAllClasses().get(i).getNoStudent() > inpNumber){
                target.add(getAllClasses().get(i));
            }
        }
        return target;
    }
}
