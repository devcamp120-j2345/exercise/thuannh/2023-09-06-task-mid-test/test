package com.devcamp.s10_schoolclassapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s10_schoolclassapi.model.School;
@Service
public class SchoolService {
    @Autowired
    public ClassroomService classroom  = new ClassroomService()     ;
    
    School group1 = new School(123988, "THPT Ngô Gia Tự", "Số 1 Nguyễn Quán Quang", classroom.getGroup1School());
    School group2 = new School(123987, "THPT Lý Thái Tổ", "52 Lý Thái Tổ", classroom.getGroup2School());
    School group3 = new School(123989, "THPT Từ Sơn", "55 Giải Phóng", classroom.getGroup3School());

//get list of schools
public ArrayList<School> getAllSchools(){
    ArrayList<School> allSchools = new ArrayList<>();
    allSchools.add(group1);
    allSchools.add(group2);
    allSchools.add(group3);

    return allSchools;
}

//get school from schools'id
public School getSchoolById(int id){
    ArrayList<School> allSchools = getAllSchools();
    School target = new School();
    for (School schoolSearch: allSchools){
        if (schoolSearch.getId() == id){
            target = schoolSearch;
        }
    } 
    return target;
}

//get school which total number of student is higher than input param number
public ArrayList<School> getSchoolByInputNoStudent(int inpNumber){
    ArrayList<School> allSchools = new ArrayList<>();
    for (int i=0; i<getAllSchools().size(); i++){
        if (getAllSchools().get(i).getTotalStudent() > inpNumber){
            allSchools.add(getAllSchools().get(i));
        }
    }
    return allSchools;
}


}
