package com.devcamp.s10_schoolclassapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S10SchoolclassapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(S10SchoolclassapiApplication.class, args);
	}

}
